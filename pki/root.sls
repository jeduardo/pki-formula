{# All the states here should be executed in the machine hosting the Root CA #}
include:
  - pki

{# Directory to keep a copy of the key infrastructure #}
/srv/pki:
  file.directory: 
    - user: root
    - group: root
    - mode: 700

{# Directory to keep a copy of the issued certificates. #}
/srv/pki/issued_certs:
  file.directory:
    - user: root
    - group: root
    - mode: 700

{# Master key for the Root CA #}
/srv/pki/rootca.key:
  x509.private_key_managed:
    - bits: 4096
    - backup: True
    - require:
      - file: /srv/pki
    {# Preventing key to be rotated when a new cert needs to be issued #}        
    {%- if salt['file.file_exists']('/srv/pki/rootca.key') %}
    - prereq:
      - x509: /srv/pki/rootca.pem
    {%- endif %}

{# The Root CA itself #}
/srv/pki/rootca.pem:
  x509.certificate_managed:
    - signing_private_key: /srv/pki/rootca.key
    - nsComment: "Issued by Salt"
    - basicConstraints: "critical CA:true"
    - keyUsage: "critical cRLSign, keyCertSign"
    - subjectKeyIdentifier: hash
    - authorityKeyIdentifier: keyid,issuer:always
    - days_valid: 3650
    - days_remaining: 30
    - backup: True
    {%- for key, value in salt['pillar.get']('pki:rootca:options').items() %}
    - {{ key }}: {{ value }}
    {%- endfor %}

{# Republish the root CA every time the formula is executed. #}
publish updated salt mine with root ca:
  module.run:
    - name: mine.send
    - func: x509.get_pem_entries
    - kwargs:
        glob_path: /srv/pki/rootca.pem
    - require:
      - x509: /srv/pki/rootca.pem
