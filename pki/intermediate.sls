# Configuration for the intermediate CA. #}
include:
  - pki

{# Directory to keep a copy of the issued certificates. #}
/srv/pki/intermediate_issued_certs:
  file.directory:
    - user: root
    - group: root
    - mode: 700

{# Master key for the Intermediate CA #}
/srv/pki/intermediateca.key:
  x509.private_key_managed:
    - bits: 4096
    - backup: True
    - new: True
    {# Preventing key to be rotated when a new cert needs to be issued #}
    {% if salt['file.file_exists']('/srv/pki/intermediateca.key') %}
    - prereq:
      - x509: /srv/pki/intermediateca.pem
    {% endif %}

{# Intermediate CA #}
/srv/pki/intermediateca.pem:
  x509.certificate_managed:
    - ca_server: {{ salt['pillar.get']('pki:rootca:minion') }}
    - public_key: /srv/pki/intermediateca.key
    - days_remaining: 30
    - backup: True
    {%- for key, value in salt['pillar.get']('pki:intermediateca:options').items() %}
    - {{ key }}: {{ value }}
    {%- endfor %}
    - signing_policy: intermediate

{# Always republish the intermediate CA #}
publish updated salt mine with intermediate ca:
  module.run:
    - name: mine.send
    - func: x509.get_pem_entries
    - kwargs:
        glob_path: /srv/pki/intermediate.pem
    - require:
      - x509: /srv/pki/intermediateca.pem
